FieldOps Docker 
===============

This Project contains the Docker compose script for standing up an instance of FieldOps 

User Guide 
----------

There is an online user guide hosted on `Read The Docs <https://fieldops.readthedocs.io/en/latest/>`_

Starting Service 
----------------

To get started, clone this repo (with submodules) and run docker compose 

.. code:: bash 
    
    git clone git@gitlab.com:cranecreeklabs/fieldops/fieldops-docker.git
    cd fieldops-docker
    docker-compose up -d

This will start the following services:

- api: The FieldOps RestAPI
- dashboard: The FieldOps WebUI
- mongo: The MongoDB Database

The dashboard will be available at http://localhost:8080 
